import React from "react";
import { Link } from "react-router-dom";
import { Button } from "./Button";
import { Rating } from "./Rating";

export const ProductCard = ({ product }) => {


  const discounted = product?.discounted_price ?( <span className="text-3xl font-bold text-primary-700 dark:text-white">
  ${product.discounted_price}
</span>):'';
console.log(product.discounted_price)
  return (
    <div className="m-2 w-full max-w-sm bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <Link to="#">
        <img
          className="p-8 rounded-t-lg"
          src={ product.poster }
          alt={ product.name }
        />
      </Link>
      <div className="px-5 pb-5">
        <Link to="#">
          <h5 className="text-lg font-semibold tracking-tight text-gray-900 dark:text-white">
            { product.name }
          </h5>
        </Link>
        <Rating rating={product.rating}/>
        <div className="flex items-center justify-between">
          <span className={`text-3xl font-bold text-gray-900 dark:text-white ${product.discounted_price?'line-through' : ''}`}>
            ${ product.price }
          </span> {discounted}
          <Link
            to="#"
          >
            <Button>
              Add to cart
            </Button>
          </Link>
        </div>
      </div>
    </div>
  );
};
