import React from 'react'
import { Route, Routes } from 'react-router-dom'
import { HomePage, Login, PrivacyPolicy, ProductDetailsPage, ProductPage, RefundPolicyPage, Register } from '../pages'

export const AppRoutes = () => {
  return (
    <>
      <Routes>
        <Route path='/' element={ <HomePage /> } />
        <Route path='/products' element={ <ProductPage /> } />
        <Route path='/products/:id' element={<ProductDetailsPage />} />
        <Route path='/refund-policy' element={ <RefundPolicyPage /> } />
        <Route path='/privacy-policy' element={ <PrivacyPolicy /> } />
        <Route path='/register' element={ <Register />} />
        <Route path='/login' element={ <Login />} />

      </Routes>
    </>
  )
}
