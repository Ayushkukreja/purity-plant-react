export { HomePage } from './Home/HomePage';
export { Faq } from './Home/components/Faq';
export { Hero } from './Home/components/Hero';
export { Testimonial } from './Home/components/Testimonial';

export { SliderCard } from './Home/components/SliderCard';
export { Accordion } from './Home/components/Accordion';
export { FeaturedProducts } from './Home/components/FeaturedProducts';

export {PrivacyPolicy} from './Policy/PrivacyPolicy'
export {RefundPolicyPage} from './Policy/RefundPolicyPage'

export { FilterBar } from './Product/components/FilterBar';
export { ProductPage } from './Product/ProductPage';
export { ProductDetailsPage } from './Product/ProductDetailsPage';

export { Register } from './Auth/Register';
export { Login } from './Auth/Login';



