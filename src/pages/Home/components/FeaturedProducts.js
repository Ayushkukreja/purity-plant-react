import React from 'react'
import { useFetch } from '../../../hooks';
import { ProductCard } from '../../../components';

export const FeaturedProducts = () => {
  const BASE_URL = process.env.REACT_APP_API_URL;
  const { data } = useFetch(`${BASE_URL}featured-products`);
  return (
    <div className="container mx-auto dark:text-white mb-16">
      
      <h2 className='text-4xl font-medium mb-4 text-center sectionTitle'><img src="https://www.pngall.com/wp-content/uploads/15/Green-Leaf-PNG-Image.png" alt="" srcset="" className='w-[60px] absolute left-[470px] -rotate-[30deg]' />Featured Products<img src="https://www.pngall.com/wp-content/uploads/15/Green-Leaf-PNG-Image.png" alt="" srcset="" className='w-[60px] absolute left-[865px] top-[615px] rotate-[160deg]' /></h2>
      <div className='flex flex-wrap justify-center'>
        { data && data.map(product => <ProductCard key={product.id} product={product} />)}
      </div>
    </div>
  )
}
