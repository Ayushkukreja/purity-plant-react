import React from "react";
import { Accordion } from "./Accordion";
import { useFetch } from "../../../hooks";

const baseurl = process.env.REACT_APP_API_URL; 

export const Faq = () => {
  const { data } = useFetch(`${baseurl}faqs`);
  const faqs = data;
  console.log(faqs)
  return (
    <div className="container mx-auto dark:text-white mb-16" data-accordion="open">
      <h2 className='text-4xl font-medium mb-4 text-center sectionTitle'>FAQs</h2>
      {data && faqs.map(faq => <Accordion key={faq.id} faq={faq} />) }
      
    </div>
  );
};
