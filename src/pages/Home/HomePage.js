import React from 'react'
import { Hero , FeaturedProducts,Testimonial,Faq } from '../index'

export const HomePage = () => {
  return (
    <main className='dark:bg-slate-800 '>
      <Hero />
      <FeaturedProducts />
      <Testimonial />
      <Faq />

    </main>
  )
}
