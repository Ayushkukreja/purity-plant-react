    import React from 'react'

    export const RefundPolicyPage = () => {
    return (
        <body class="container mx-auto bg-primary-50 dark:bg-slate-800 dark:text-white">

        <div class="mt-16 p-8 rounded shadow-md">

            <h1 class="text-3xl font-bold mb-10 text-primary-800">Refund Policy for Purity Plants</h1>

            <p class="mb-6">Thank you for shopping at Purity Plants. We understand that sometimes the plant you purchased may
                not meet your expectations. This Refund Policy explains how we handle refunds and returns.</p>

            <h2 class="text-2xl font-bold mb-6 text-primary-800">Eligibility</h2>

            <p class="mb-6">To be eligible for a refund, the plant must be in the same condition that you received it and must
                be returned within 7 days of the purchase.</p>

            <h2 class="text-2xl font-bold mb-6 text-primary-800">Refund Process</h2>

            <p class="mb-6">Once we receive your return, we will inspect it and notify you about the status of your refund.
                If your return is approved, we will initiate a refund to your original payment method.</p>

            <h2 class="text-2xl font-bold mb-6 text-primary-800">Shipping Costs</h2>

            <p class="mb-6">Shipping costs are non-refundable. If you receive a refund, the cost of return shipping will be
                deducted from your refund.</p>

            <h2 class="text-2xl font-bold mb-6 text-primary-800">Contact Us</h2>

            <p class="mb-6">If you have any questions on how to return your item to us, contact us at
                <a href="mailto:returns@purityplants.com" class="text-blue-500">returns@purityplants.com</a>.</p>

            <p class="text-2xl text-primary-800">Thank you for shopping with Purity Plants. We appreciate your trust in our
                products.</p>

        </div>

    </body>
    )
    }
