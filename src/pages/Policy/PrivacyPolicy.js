import React from 'react'

export const PrivacyPolicy = () => {
  return (
    <main class="container mx-auto bg-primary-50 dark:bg-slate-800 dark:text-white">
    <section class="mt-16 p-8 rounded shadow-md">
      <h2 class="text-3xl font-bold mb-10 text-primary-800">Introduction</h2>
      <p  class="mb-6">Welcome to our Privacy Policy page. This policy explains how we collect, use, disclose, and safeguard your information when you visit our website or make a purchase from us.</p>
    

    
      <h2 class="text-3xl font-bold mb-10 text-primary-800">Information We Collect</h2>
      <p  class="mb-6">We collect various types of information for different purposes to provide and improve our services to you. The types of information we collect may include:</p>
      <ul class="mb-6">
        <li>Contact information (such as name, email address, phone number).</li>
        <li>Payment and billing information.</li>
        <li>Information you voluntarily provide in forms, surveys, or contests.</li>
        <li>Information automatically collected through cookies and similar technologies.</li>
        <li>Device information (such as IP address, browser type, device type).</li>
      </ul>
      {/* <!-- More content about information collection --> */}
    
      <h2 class="text-3xl font-bold mb-10 text-primary-800">How We Use Your Information</h2>
      <p  class="mb-6">We may use the information collected for various purposes, including:</p>
      <ul class="mb-6">
        <li>Providing, maintaining, and improving our services.</li>
        <li>Communicating with you and fulfilling your requests.</li>
        <li>Personalizing your experience and delivering targeted content and advertisements.</li>
        <li>Preventing fraudulent activities and ensuring security.</li>
      </ul>
      {/* <!-- More content about information usage --> */}
    </section>

    {/* <!-- Additional sections about data sharing, cookies, user rights, etc. --> */}

  </main>
  )
}
