import React from 'react'
import { Rating } from '../../components';

export const ProductDetailsPage = () => {
  
  return (
    
    <main>
        <section>
          <h1 className="mt-10 mb-5 text-4xl text-center font-bold text-slate-900 dark:text-slate-200">Snake Plant</h1>
          <p className="mb-5 text-lg text-center text-slate-900 dark:text-slate-200">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ratione, tempora.</p>
          <div className="flex flex-wrap justify-around">
            <div className="max-w-xl my-3">
              <img className="rounded" src="/product-images/1.webp" alt="Product Name" />
            </div>
            <div className="max-w-xl my-3">
              <p className="text-3xl font-bold text-gray-900 dark:text-slate-200">
                <span className="mr-1">$</span>
                <span className="">{45}</span>
              </p>
              <p className="my-3"> 
                <span>
                  <Rating rating={4} />
                </span>
              </p>
              <p className="my-4 select-none">
                <span className="font-semibold text-amber-500 border bg-amber-50 rounded-lg px-3 py-1 mr-2">BEST SELLER</span> 
                <span className="font-semibold text-emerald-600	border bg-slate-100 rounded-lg px-3 py-1 mr-2">INSTOCK</span>
                <span className="font-semibold text-rose-700 border bg-slate-100 rounded-lg px-3 py-1 mr-2">OUT OF STOCK</span>
                <span className="font-semibold text-blue-500 border bg-slate-100 rounded-lg px-3 py-1 mr-2">7 Inches</span>
              </p>
              <p className="my-3">
                <button className={`inline-flex items-center py-2 px-5 text-lg font-medium text-center text-white bg-primary-700 rounded-lg hover:bg-primary-900 cursor-not-allowed}`} disabled>Add To Cart <i className="ml-1 bi bi-plus-lg"></i></button> 
              
              </p>
              <p className="text-md text-gray-900 dark:text-slate-200">
                Lorem ipsum dolor, sit amet consectetur adipisicing elit. Earum quibusdam possimus placeat perspiciatis eum. Delectus commodi iure voluptatum est animi? Provident, dignissimos at et id accusantium voluptatum maiores dicta praesentium in odit commodi enim eos? Quod possimus fugit eligendi hic. Quisquam adipisci rerum consectetur earum in. Voluptas facere repudiandae provident?
              </p>
            </div>
          </div>
        </section>
      </main> 
    
  )
}
