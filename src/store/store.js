import { configureStore } from "@reduxjs/toolkit";
import productreducers from "./productslice";

export default configureStore({
    reducer:{
        products:productreducers
    }
})